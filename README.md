# feditrends
Post to Mastodon server top ten most used fediverse's trends of the current week.  
It gets current week trends from all Mastodon servers of the fediverse and then post top ten most used ones via your Mastodon server's bot account.


### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon server (admin)
-   fediverse bot running. Get it here -> https://gitlab.com/spla/fediverse

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed Python libraries.

2. Run `python db-setup.py` to create feditrends's needed database and table.

3. Run `python setup.py` to get your Mastodon's access token of an existing user. It will be saved to 'secrets/secrets.txt' for further use.

4. Run `python feditrends.py` to start getting all fediverse's hashtags 

5. Use your favourite scheduling method to set feditrends.py to run regularly.

