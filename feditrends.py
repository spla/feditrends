import time
from datetime import datetime, timedelta
from mastodon import Mastodon
import re
import os
import json
import sys
import os.path
import requests
import psycopg2
import ray

ray.init(num_cpus = 32) # Specify this system CPUs.

def is_json(myjson):

    try:
        json_object = json.loads(myjson)
    except ValueError as e:
        return False
    return True

def tooting(checked_servers, mastodon):

    toot_hashtag = []
    toot_uses = []
    toot_users = []
    toot_servers = []

    conn = None

    try:

        conn = psycopg2.connect(database = trends_db, user = trends_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select count(*) from trends")

        row = cur.fetchone()

        total_tags = row[0]

        cur.execute("select hashtag, servers, uses, users from trends order by servers desc limit 10")

        rows = cur.fetchall()

        for row in rows:

            toot_hashtag.append(row[0])
            toot_servers.append(row[1])
            toot_uses.append(row[2])
            toot_users.append(row[3])

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    toot_text = "Fediverse's top ten most used hashtags (current week)\n"
    toot_text += f'\nChecked servers: {str(checked_servers)}\n'
    toot_text += f'Total hashtags: {str(total_tags)}\n'
    toot_text += f'\nResults: \n'
    toot_text += '(hashtag / servers / uses / users)\n\n'

    i = 0
    while i < len(toot_hashtag):

        toot_text += f"{str(toot_hashtag[i])} / {str(toot_servers[i])} / {str(toot_uses[i])} / {str(toot_users[i])}\n"

        i += 1

    print(toot_text)

    print('Tooting')
    mastodon.status_post(toot_text, in_reply_to_id=None, )

    return

def delete_trends():

    conn = None

    try:

        conn = psycopg2.connect(database = trends_db, user = trends_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("delete from trends")

        conn.commit()

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def get_servers_trends():

    trends_servers = []

    conn = None

    try:

        conn = psycopg2.connect(database = 'fediverse', user = trends_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select server from fediverse where software='mastodon' and alive")

        rows = cur.fetchall()

        for row in rows:

            server = row[0]
            trends_servers.append(server)

        cur.close()

        return trends_servers

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

@ray.remote
def get_trends(server):

        try:

            s = requests.Session()

            s.headers['User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36'

            res = s.get('https://' + server + '/api/v1/trends', allow_redirects=False, timeout=3)

        except requests.exceptions.ConnectionError as conn_error:

            pass
            return

        except requests.exceptions.ReadTimeout as read_error:

            pass
            return

        except requests.exceptions.ConnectTimeout as conn_timeout:

            pass
            return

        if is_json(res.text):

            print(server + " OK")
            data = res.json()

            if len(data) == 1:

                return

            trend_name = []
            trend_uses = []
            trend_users = []

            i1 = 0
            while i1 < len(data):

                trend_name.append(data[i1]['name'])
                trend_uses.append(int(data[i1]['history'][0]['uses']))
                trend_users.append(int(data[i1]['history'][0]['accounts']))

                i1 += 1

            i2 = 0
            while i2 < len(trend_name):

                hashtag = trend_name[i2]
                uses = trend_uses[i2]
                users = trend_users[i2]
                servers = 1

                insert_sql = "INSERT INTO trends(hashtag, uses, users, servers) VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"

                conn = None

                try:

                    conn = psycopg2.connect(database = trends_db, user = trends_db_user, password = "", host = "/var/run/postgresql", port = "5432")

                    cur = conn.cursor()

                    cur.execute("select uses, users, servers from trends where hashtag=(%s)", (hashtag,))

                    row = cur.fetchone()

                    if row != None:

                        partial_uses = row[0]
                        partial_users = row[1]
                        partial_servers = row[2]


                        uses = partial_uses + uses
                        users = partial_users + users
                        servers = partial_servers + 1


                    cur.execute(insert_sql, (hashtag, uses, users, servers))

                    cur.execute("UPDATE trends SET uses=(%s), users=(%s), servers=(%s) where hashtag=(%s)", (uses, users, servers, hashtag))

                    conn.commit()

                    cur.close()

                except (Exception, psycopg2.DatabaseError) as error:

                    print(error)

                finally:

                    if conn is not None:

                        conn.close()
                i2 += 1

        else:

            print(f"* {server} don't reply API requests")
            return

        return

def mastodon():

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    return (mastodon, mastodon_hostname)

def db_config():

    # Load db configuration from config file
    config_filepath = "config/db_config.txt"
    trends_db = get_parameter("trends_db", config_filepath)
    trends_db_user = get_parameter("trends_db_user", config_filepath)

    return (trends_db, trends_db_user)

def get_parameter( parameter, file_path ):

    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

###############################################################################
# main

if __name__ == '__main__':

    mastodon, mastodon_hostname = mastodon()

    trends_db, trends_db_user = db_config()

    trends_servers = get_servers_trends()

    servers_checked = len(trends_servers)

    delete_trends()

    ray_start = time.time()

    results = ray.get([get_trends.remote(server) for server in trends_servers])

    print(f"duration = {time.time() - ray_start}.\nprocessed servers: {len(results)}")

    get_trends.remote(mastodon_hostname)

    tooting(servers_checked, mastodon)

