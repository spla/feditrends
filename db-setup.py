#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getpass
import os
import sys
from mastodon import Mastodon
from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout, MastodonAPIError
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
    if not os.path.exists('config'):
        os.makedirs('config')
    print("Setting up trends parameters...")
    print("\n")
    trends_db = input("trends db name: ")
    trends_db_user = input("trends db user: ")

    with open(file_path, "w") as text_file:
        print("trends_db: {}".format(trends_db), file=text_file)
        print("trends_db_user: {}".format(trends_db_user), file=text_file)

def create_table(db, db_user, table, sql):

    conn = None

    try:

        conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
        cur = conn.cursor()

        print("Creating table.. "+table)

        cur.execute(sql)

        conn.commit()
        print("Table "+table+" created!")
        print("\n")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

###############################################################################
# main

if __name__ == '__main__':

    # Load configuration from config file
    config_filepath = "config/db_config.txt"
    trends_db = get_parameter("trends_db", config_filepath)
    trends_db_user = get_parameter("trends_db_user", config_filepath)

    ############################################################
    # create database
    ############################################################

    conn = None

    try:

        conn = psycopg2.connect(dbname='postgres',
            user=trends_db_user, host='',
            password='')

        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

        cur = conn.cursor()

        print("Creating database " + trends_db + ". Please wait...")

        cur.execute(sql.SQL("CREATE DATABASE {}").format(
            sql.Identifier(trends_db))
        )
        print("Database " + trends_db + " created!")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    ############################################################

    try:

        conn = None

        conn = psycopg2.connect(database = trends_db, user = trends_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

        # Load configuration from config file
        os.remove("config/db_config.txt")

        print("Exiting. Run db-setup again with right parameters")
        sys.exit(0)

    finally:

        if conn is not None:

            conn.close()

            print("\n")
            print("trends parameters saved to db-config.txt!")
            print("\n")

    ############################################################
    # Create needed tables
    ############################################################

    print("Creating table...")

    db = trends_db
    db_user = trends_db_user
    table = "trends"
    sql = 'create table '+table+'(hashtag varchar(40) PRIMARY KEY, uses int, users int, servers int)'
    create_table(db, db_user, table, sql)

    ############################################################

    print("Done!")
    print("Now you can run setup.py!")
    print("\n")
